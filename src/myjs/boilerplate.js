"use strict";


(function()  {
	var items = $('#slider > img');
	var count = items.size();
	var current = 0;
	var next = 1;

	// setInterval(function() {
	// 	items.hide();
	// 	$('#pager > a').removeClass('activeSlide');

	// 	if(next >= count) {
	// 		next = 0;
	// 		current = 0;
	// 	}
	// 	$('#slider > img:eq('+next+')').fadeIn(300);
	// 	$('#pager > a:eq('+next+')').addClass('activeSlide');
	// 	current = next;
	// 	next = next + 1;
	// },3000);


	// nut next
	$('#next').click(function() {
		slide(current + 1);
	});

	$('#pre').click(function() {
		slide(current - 1);
	});

	function slide(index) {
		if(index >= count) index = 0;
		if(index < 0) index = count - 1;

		items.hide();
		$('#pager > button').removeClass('activeSlide');

		$('#slider > img:eq('+ index + ')').fadeIn(300);
		$('#pager > button:eq(' + index + ')').addClass('activeSlide');
		current = index;
	}

	var pager = '<button></button>';
	
	items.each(function() {
		$('#pager').append(pager);
	});

	$.each($('#pager button'), function(index, value){
		console.log(index);
		$(this).click(function(e){
			e.preventDefault();
			$('#pager button').removeClass('activeSlide'); 
 			$(this).addClass('activeSlide');
 			slide($(this).index());
		});
	});

	/*$('#pager a').click(function(e){
		e.preventDefault();
   		$('#pager a').removeClass('activeSlide'); 
 		$(this).addClass('activeSlide');
 		slide($(this).index());
	});*/



	$('#pager > button').eq(0).addClass('activeSlide');


}());