"use strict";

/*(function() {
	console.log("Crockford");
}());

// (function(){*some code*})()
(function() {
	console.log("this work!");
})();

var i = function() { return 10; }();
console.log(i);

true && function() {
	console.log("true");
}();

0, function() {
	console.log("0");
}();

!function() {
	console.log("!save bytes");
}();

~function(){
	console.log("~save bytes");
}();

-function() {
	console.log("-save bytes");
}();

+function() {
	console.log("+save bytes");
}();

new function() {

	console.log("use new");
};

new function() {
	console.log("use new ()");
}*/

// $('.slider').cycle({
// 	fx: 'scrollHorz',
// 	next: '.next',
// 	prev: '.pre',
// 	pager: '.pager',
// 	timeout: 3000,
// 	speed: 900
// });
;(function ($, window, document, undefined) {

	// not extend
	var pluginName = "slider",
		dataKey = "plugin_" + pluginName;

	var Plugin = function (element, options) {
		this.element = element;

		this.options = {
			current: 0,
			speed: 300,
			image_names: ["1", "2", "3", "4"]
		};

		this.init(options);
	};

	Plugin.prototype = {
		init: function (options) {
			// options can change and extend
			$.extend(this.options, options);

			// SOME VAR IS CHANGED TO USE
			//this.options.image_names = ["1", "2", "3", "4"];
			this.options.speed = 500;
			var that = this;

			//SETUP: LOAD ALL ELEMENTS
			this.setup();

			// SLIDER AUTO RUN
			setInterval(function() {
				// remove all
				that.element.find('img').hide();
				$('.pager > button').removeClass('activeSlide');

				++that.options.current;
				if(that.options.current >= that.options.image_names.length) {
					that.options.current = 0;
				}
				that.element.find('img').eq(that.options.current).fadeIn(that.options.speed);
				$('.pager > button').eq(that.options.current).addClass('activeSlide');
			},3000);

			//hide all images
			this.element.find('img').hide();

			// current img is showed
			this.element.find('img').eq(this.options.current).fadeIn(this.options.speed);
			// current pager item is actived
			$('.pager button').eq(this.options.current).addClass('activeSlide');
		},

		next: function() {
			this.slide(this.options.current + 1);
		},

		prev: function(){
			this.slide(this.options.current - 1);
		},

		slide: function(index){
			// current img > total -> set to first img
			if(index >= this.element.find('img').length) index = 0;

			// current img < 0 -> set to last img
			if(index < 0) index = this.element.find('img').length - 1;

			// hide all imgs
			this.element.find('img').hide();
			// remove actived pagers
			$('.pager button').removeClass('activeSlide');

			// current img is showed
			this.element.find('img').eq(index).fadeIn(300);
			// current pager is actived
			$('.pager button').eq(index).addClass('activeSlide');

			this.options.current = index;
		},

		setup: function(){
			var that = this;
			$.each(this.options.image_names, (index, value) => {
				// setting images
				this.element.append('<img src="../src/images/'+ value +'.jpg" alt="'+ value +'">');

				//setting pager
				$('.pager').append('<button></button>');
				$('.pager button').eq(index).click(function(){
					$('.pager button').removeClass('activeSlide');
					$(this).addClass('activeSlide');
					that.slide($(this).index());
				});
			});
		}
	};

	$.fn[pluginName] = function (options) {

		var plugin = this.data(dataKey);

		if(plugin instanceof Plugin){
			if(typeof options !== 'undefined'){
				plugin.init(options);
			}
		} else{
			plugin = new Plugin(this, options);
			this.data(dataKey, plugin);
		}

		return plugin;
	};
}(jQuery, window, document) );

// setup begin
$('.slider').slider();


